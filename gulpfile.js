var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('css', function() {
	gulp.src('scss/main.scss')
    .pipe(sass())
    .pipe(gulp.dest('dist'))
});

gulp.task('watch', function(){
	gulp.watch('scss/*.scss', ['css']);
})

gulp.task('default', ['css']);
